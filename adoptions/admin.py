from django.contrib import admin
from .models import Pet, Vaccine


@admin.register(Pet)
class PetAdmin(admin.ModelAdmin):
    list_display = ['name', 'breed', 'species', 'sex']


admin.site.register(Vaccine)
