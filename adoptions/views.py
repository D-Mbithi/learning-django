from django.shortcuts import render
from .models import Pet

def home(request):
    template = 'home.html'
    pets = Pet.objects.all()

    context = {'pets': pets}

    return render (request, template, context)

def pet_detail(request, pet_id):
    pet = Pet.objects.get(id=pet_id)
    template = 'pet_detail.html'
    context = {'pet':pet}

    return render (request, template, context)
